# RESTCONF

## What is RESTCONF?

**RESTCONF** is a network management protocol that provides a programmatic
interface for accessing configuration data, state data, RPC operations, and
event notifications defined in **YANG** within a networking
device.

## What is the relationship between RESTCONF and NETCONF?

RESTCONF uses HTTP methods to implement the equivalent of
**NETCONF** **CRUD** operations (Create, Read, Update, Delete) on a conceptual datastore containing data defined in YANG. RESTCONF does not need to mirror the full functionality of the NETCONF protocol, but it does need to be compatible with NETCONF.

RESTCONF is not intended to replace NETCONF, but rather to provide an HTTP
interface that follows **Representational State Transfer (REST)** principles and
is compatible with the NETCONF datastore model.

## Messages

The RESTCONF protocol uses HTTP messages. A single HTTP message corresponds to a
single protocol method. Most messages can perform a single task on a single
resource, such as retrieving a resource or editing a resource.

The RESTCONF uses the HTTP **GET**, **POST**, **PUT**, **PATCH**, and **DELETE**
methods to retrieve/ edit data resources within a networking device. The
following table shows how the RESTCONF use HTTP methods to implement the
equivalent of NETCONF CRUD operations.

| **RESTCONF** | **NETCONF**                                     |
|--------------|-------------------------------------------------|
| GET          | \<get-config\>, \<get\>                         |
| POST         | \<edit-config\> (nc:operation=”create”)         |
| PUT          | \<edit-config\> (nc:operation=”create/replace”) |
| PATCH        | \<edit-config\> (nc:operation=”merge”)          |
| DELETE       | \<edit-config\> (nc:operation=”delete”)         |

## URI Structure

A RESTCONF operation is derived from the HTTP method and the request URI. The
request URI is like below:

    https://<server-address>/<entry>/<path>?<query>

**entry**: The root of the RESTCONF API configured on this HTTP server, discovered by getting the “https://<server-address>/.well-known/host-meta” resource.

**path**: The path expression identifying the resource that is being accessed by the RESTCONF operation.

**query**: The set of parameters associated with the RESTCONF message. RESTCONF parameters have the familiar form of “name=value” pairs.

## Message Encoding

RESTCONF messages are encoded in HTTP and message content is encoded in either JSON or XML format. A server MUST support one of either XML or JSON encoding.

### Example in XML

Request:

    GET /restconf/data/interfaces/interface=eth1?with-defaults=report-all-tagged HTTP/1.1 
    Host: example.com 
    Accept: application/yang-data+xml 

Response:

    HTTP/1.1 200 OK
    Date: Thu, 26 Jan 2017 20:56:30 GMT
    Server: example-server
    Content-Type: application/yang-data+xml
    <interface xmlns="urn:example.com:params:xml:ns:yang:example-interface">
    <name>eth1</name>
    <mtu xmlns:wd="urn:ietf:params:xml:ns:netconf:default:1.0" wd:default="true">1500</mtu>
    <status>up</status>
    </interface>

### Example in JSON

Request:

    GET /restconf/data/interfaces/interface=eth1\\?with-defaults=report-all-tagged HTTP/1.1
    Host: example.com
    Accept: application/yang-data+json

Response:

    HTTP/1.1 200 OK
    Date: Thu, 26 Jan 2017 20:56:30 GMT
    Server: example-server
    Content-Type: application/yang-data+json
    {
        "example:interface" : [
            {
                "name" : "eth1",
                "mtu" : 1500,
                "@mtu" : {
                    "ietf-netconf-with-defaults:default" : true
                },
                "status" : "up"
            }
        ]
    }

## Configuration Lock and Rollback

RESTCONF does not have lock capability, but the transactional behavior is guaranteed within one request. Since each RESTCONF request is a transaction, any error in it will cause a config roll back.

## Reference:
<https://tools.ietf.org/html/rfc8040>
